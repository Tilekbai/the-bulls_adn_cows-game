from django.apps import AppConfig


class BullappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bullapp'
